const express = require('express');
const path = require('path');

const app = express(); 
const port = 3100;

app.use(express.static(path.join(__dirname, '../build')));

app.get('/*',(req, res) => {
//   console.log(process.env.REACT_APP_LOGIN_API);
//   console.log(process.env.REACT_APP_LIBRARY_API);
  res.sendFile(path.join(__dirname, '../build/index.html'));
});

app.listen(port, () => {
    console.log('server started at port:'+port);
});