import React, { Component } from 'react';
import { connect } from 'react-redux';
import { editBook } from '../../actions/bookAction';

 class UpdateBook extends Component {
     constructor(props){
        super(props);
        this.onInputChange = this.onInputChange.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.state = {
            name:"",
            ISBN:"",
            paperback:"",
            review:"",
            publisher:"",
            imgLink:"",
            description:"",
            link:"",
            authorId:""
        }
     }
     onInputChange(event){
        this.setState({
            [event.target.name]:event.target.value
        })
        // set authorName 
        if(event.target.type === "radio"){
            this.setState({
                authorName:event.target.getAttribute('data')
            });
        }
     }
     onFormSubmit(event){
        event.preventDefault();
        this.props.editBook(this.state);
        this.setState({
            isAppended:true
        });
        console.log(this.state);
    }
    componentDidMount(){
        const bookData = this.props.bookData;
        if(bookData){
            this.setState({
                ...bookData
            });
        }
    }
  render() {
    let radioButtons;
        const { authorsData } =this.props;
        if(authorsData){
            radioButtons = authorRadio(authorsData, this.onInputChange);
        }
        return(
            <div className="container mt-5">
                <div className="row">
                    <div className="col-md-12">
                        <div className="well well-sm">
                            <form className="form-horizontal" onSubmit= {this.onFormSubmit} >
                                <fieldset>
                                    <legend className="text-center header">Add Book</legend>
                                    <div className="form-group">
                                        <div className="col-md-8">
                                            <label htmlFor="name" className="form-label">Name</label>
                                            <input id="name" value={this.state.name} onChange={this.onInputChange} name="name" type="text" placeholder="Name" className="form-control" required/>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="col-md-8">
                                            <label htmlFor="ISBN" className="form-label">ISBN</label>
                                            <input id="ISBN" value={this.state.ISBN} onChange={this.onInputChange} name="ISBN" type="number" className="form-control" required/>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="col-md-8">
                                            <label htmlFor="paperback" className="form-label">No.of pages</label>
                                            <input id="paperback" value={this.state.paperback} onChange={this.onInputChange} name="paperback" type="number" className="form-control" required/>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="col-md-8">
                                            <label htmlFor="review" className="form-label">Reviews</label>
                                            <input id="review" value={this.state.review} onChange={this.onInputChange} name="review" type="text" className="form-control" placeholder="4.0 stars" required/>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="col-md-8">
                                            <label htmlFor="publisher">publisher</label>
                                            <input id="publisher" value={this.state.publisher} onChange={this.onInputChange} name="publisher" type="text" placeholder="publisher" className="form-control" required/>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="col-md-8">
                                            <label htmlFor="imgLink" className="form-label">Image-url</label>
                                            <input id="imgLink" value={this.state.imgLink} onChange={this.onInputChange} name="imgLink" type="text" className="form-control" required/>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="col-md-8">
                                            <label htmlFor="description">Description</label>
                                            <textarea value={this.state.description} onChange={this.onInputChange} className="form-control" id="description" name="description" placeholder="Enter the book description" rows="7" required></textarea>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="col-md-8">
                                            <label htmlFor="book-link" className="form-label">Book-link</label>
                                            <input id="link" value={this.state.link} onChange={this.onInputChange} name="link" type="text" className="form-control" required />
                                        </div>
                                    </div>
                                    <div className="form-group ml-3" id="radio">
                                        <p>Choose Author </p>
                                        { radioButtons }
                                    </div>
                                    <div className="form-group">
                                        <div className="col-md-8">
                                        <button id="submit" type="submit" className="btn btn-primary btn-lg">Submit</button>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                        <div className="modal" >
                           {this.state.isAppended ? (<h2> The data has been appended.. </h2>) :<h2> false </h2>  }
                        </div>  
                    </div>
                </div>
            </div>
        );
  }
}
function authorRadio(authorsData, onInputChange){
    const radioButtons = authorsData.map((authorData,index) => {
        let radioButton = <div className="form-check" key = {index+1}>
        <label className="form-check-label" htmlFor={index+1}>
          <input type="radio" onChange={onInputChange} className="form-check-input" id={index+1} name="authorId" value={authorData.id} data = {authorData.name.split('_').join(' ')} required />{authorData.name.split('_').join(' ')}
        </label>
      </div>;
      return radioButton;
    });
    return radioButtons;
}    
export default connect(null,{ editBook })(UpdateBook);