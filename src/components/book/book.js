import React from 'react';
import { NavLink } from 'react-router-dom';

class Book extends React.Component{
    constructor(props){
        super(props);
        this.handleDeleteChange = this.handleDeleteChange.bind(this);
        this.handleEditChange = this.handleEditChange.bind(this);
    }
    handleDeleteChange(event){
        this.props.deleteBook(event.currentTarget.value);
    }
    handleEditChange(event){
        this.props.getBookByISBN(event.currentTarget.previousSibling.value);
    }
    render(){
        let elements = [];
        const { booksData } = this.props;
        if(booksData !== null){
            elements = createTbodyElement(booksData, this.handleEditChange, this.handleDeleteChange);
        }
        return(
            <div className ="table">
                <div className="header mt-5">
                    <h1 className="text-center">Books Section</h1>       
                </div>
                <NavLink to="/newBook" className ="btn fix-pos mt-3">New Book</NavLink>
                <table className="table table-striped mt-3">
                    <thead>
                    <tr>
                        <th scope="col">Book Cover</th>
                        <th scope="col">Name</th>
                        <th scope="col">Writer</th>
                        <th scope="col" className="text-center">Mini Description</th>
                        <th scope="col">ISBN</th>
                        <th scope="col">Review</th>
                        <th scope="col">Operation</th>
                    </tr>
                    </thead>
                    <tbody> 
                        { elements }
                    </tbody>
                </table>               
            </div>
        );
    }
}

function createTbodyElement(booksData,handleEditChange ,handleDeleteChange){
    const elements = booksData.map((bookData,index) =>{
        let element = <tr key= {(index+1).toString()}>
            <td>
               <img src= {bookData['imgLink']} alt= {bookData['name']} />
            </td>
            <td>{bookData['name']}</td>
            <td>{bookData['authorName']}</td>
            <td className="text-justify">{bookData['description'].slice(0, 150)} </td>
            <td className="ISBN">{bookData['ISBN']}</td>
            <td>{bookData['review']}</td>
            <td>
                <p title="Edit">
                    <input type="hidden" name="id" value={bookData['ISBN']} />
                    <NavLink to="/updateBook"  onClick ={ handleEditChange } value={bookData['ISBN'].toString()} className="btn btn-primary btn-xs"><i className="fa fa-pencil"></i> </NavLink>
                </p>
                <p title="Delete">
                    <button className="btn btn-danger mt-2" onClick ={ handleDeleteChange } value={bookData['ISBN'].toString()}><i className="fa fa-trash"></i></button>
                </p>
            </td>
        </tr>
        return element;
    });
    return elements;
}

export default Book;