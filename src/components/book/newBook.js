import React from 'react';
import  { connect } from 'react-redux';
import { addBook } from '../../actions/bookAction';

class NewBook extends React.Component {
    constructor(props){
        super(props);
        this.onInputChange = this.onInputChange.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.state = {
           name:"",
           ISBN:"",
           paperback:"",
           review:"",
           publisher:"",
           imgLink:"",
           description:"",
           link:"",
           authorId:""
        }
    }
    onInputChange(event){
        this.setState({
            [event.target.name]:event.target.value
        })
    }
    onFormSubmit(event){
        event.preventDefault();
        this.props.addBook(this.state);
    }
    render(){
        let radioButtons;
        const { authorsData } =this.props;
        if(authorsData){
            radioButtons = authorsData.map((authorData,index) => {
                let radioButton = <div className="form-check" key = {index+1}>
                <label className="form-check-label" htmlFor={index+1}>
                  <input type="radio" onChange={this.onInputChange} className="form-check-input" id={index+1} name="authorId" value={authorData.id} required />{authorData.name.split('_').join(' ')}
                </label>
              </div>;
              return (radioButton)
            });
        }
        return(
            <div className="container mt-5">
                <div className="row">
                    <div className="col-md-12">
                        <div className="well well-sm">
                            <form className="form-horizontal" onSubmit= {this.onFormSubmit} >
                                <fieldset>
                                    <legend className="text-center header">Add Book</legend>
                                    <div className="form-group">
                                        <div className="col-md-8">
                                            <label htmlFor="name" className="form-label">Name</label>
                                            <input id="name" onChange={this.onInputChange} name="name" type="text" placeholder="Name" className="form-control" required/>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="col-md-8">
                                            <label htmlFor="ISBN" className="form-label">ISBN</label>
                                            <input id="ISBN" onChange={this.onInputChange} name="ISBN" type="number" className="form-control" required/>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="col-md-8">
                                            <label htmlFor="paperback" className="form-label">No.of pages</label>
                                            <input id="paperback" onChange={this.onInputChange} name="paperback" type="number" className="form-control" required/>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="col-md-8">
                                            <label htmlFor="review" className="form-label">Reviews</label>
                                            <input id="review" onChange={this.onInputChange} name="review" type="text" className="form-control" placeholder="4.0 stars" required/>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="col-md-8">
                                            <label htmlFor="publisher">publisher</label>
                                            <input id="publisher" onChange={this.onInputChange} name="publisher" type="text" placeholder="publisher" className="form-control" required/>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="col-md-8">
                                            <label htmlFor="imgLink" className="form-label">Image-url</label>
                                            <input id="imgLink" onChange={this.onInputChange} name="imgLink" type="text" className="form-control" required/>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="col-md-8">
                                            <label htmlFor="description">Description</label>
                                            <textarea onChange={this.onInputChange} className="form-control" id="description" name="description" placeholder="Enter the book description" rows="7" required></textarea>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="col-md-8">
                                        <label htmlFor="book-link" className="form-label">Book-link</label>
                                        <input id="link" onChange={this.onInputChange} name="link" type="text" className="form-control" required />
                                    </div>
                                    </div>
                                    <div className="form-group ml-3" id="radio">
                                        <p>Choose Author </p>
                                        { radioButtons }
                                    </div>
                                    <div className="form-group">
                                        <div className="col-md-8">
                                        <button id="submit" type="submit" className="btn btn-primary btn-lg">Submit</button>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                
                        <div className="modal fade" id="myModal">
                            <div className="modal-dialog">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h4 className="modal-title">Database row Update</h4>
                                        <button type="button" className="close" data-dismiss="modal">&times;</button>
                                        </div>
                                    <div className="modal-body">
                                    Congrats!!
                                    The given Book data has been added into the database
                                    </div>
                                    <div className="modal-footer">
                                        <a href="/book" className="btn btn-primary">Books Section</a>
                                        <button type="button" className="btn btn-danger" data-toggle="modal" data-target="#myModal" id="target" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(null,{ addBook })(NewBook);