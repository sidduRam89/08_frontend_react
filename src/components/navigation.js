import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
class Navigation extends Component {
  render() {
    return (
        <div className="container d-flex justify-content-end mt-2 fix-pos">
          <NavLink to = "/book" className="btn">Book</NavLink><span>/</span>
          <NavLink to = "/author" className="btn">Author</NavLink> 
        </div>
    );
  }
}

export default Navigation;
