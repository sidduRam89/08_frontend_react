import React from 'react';
import { NavLink} from 'react-router-dom';
import { connect } from 'react-redux';
import { addAuthor } from '../../actions/authorAction';

class NewAuthor extends React.Component{
    constructor(props){
        super(props);
        this.popupSubmit = React.createRef();
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
        this.state = {
           name:"",
           description:"",
           born:"",
           died:"",
           occupation:"",
           imgLink:"",
           link:"",
           book:null,
        }
    }
    onInputChange(event){
        this.setState({
            [event.target.name]:event.target.value
        });
    }
    onFormSubmit(event){
        event.preventDefault();
        this.props.addAuthor(this.state);
    }
    render(){
        const { booksData } = this.props;
        let bookSelectionList = [];
        if(booksData)
            bookSelectionList = bookOptions(booksData);
        return(
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <div className="well well-sm">
                            <form className="form-horizontal" onSubmit = {this.onFormSubmit}>
                                <fieldset>
                                    <legend className="text-center header">Add Author</legend>
                                    <div className="form-group">
                                        <div className="col-md-8">
                                            <label htmlFor="name" className="form-label">Name</label>
                                            <input id="name" name="name" onChange={this.onInputChange} type="text" placeholder="Name" className="form-control" required />
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="col-md-8">
                                            <label htmlFor="imgLink" className="form-label">Image-url</label>
                                            <input id="imgLink" onChange={this.onInputChange} name="imgLink" type="text" className="form-control" />
                                        </div>
                                    </div>    
                                    <div className="form-group">
                                        <div className="col-md-8">
                                            <label htmlFor="born" className="form-label">Birth Date</label>
                                            <input id="born"  onChange={this.onInputChange} name="born" type="text" className="form-control" required />
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="col-md-8">
                                            <label htmlFor="died" className="form-label">Died on</label>
                                            <input id="died" onChange={this.onInputChange} name="died" type="text" className="form-control" required />
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="col-md-8">
                                            <label htmlFor="occupation">Occupations</label>
                                            <input id="occupation" onChange={this.onInputChange} name="occupation" type="text" placeholder="occupation" className="form-control" required />
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="col-md-8">
                                            <label htmlFor="description">Description</label>
                                            <textarea className="form-control" onChange={this.onInputChange} id="description" name="description" placeholder="Enter the author description" rows="7" required></textarea>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="col-md-8">
                                            <label htmlFor="link" className="form-label">Author-link</label>
                                            <input id="link" onChange={this.onInputChange} name="link" type="text" className="form-control" required />
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="col-md-8">
                                            <label htmlFor ="book">Select book </label> 
                                            <select id="book" onChange={this.onInputChange} className="form-control" name="book">
                                                  { bookSelectionList }
                                            </select>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="col-md-12">
                                        <button id="submit" type="submit" className="btn btn-primary btn-lg">Submit</button>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>

                        <div className="modal fade" id="myModal">
                            <div className="modal-dialog">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h4 className="modal-title">Database row Update</h4>
                                        <button type="button" className="close" data-dismiss="modal">&times;</button>
                                    </div>

                                    <div className="modal-body">
                                        Congrats!!
                                        The given author data has been added into the database
                                    </div>
                    
                                    <div className="modal-footer">
                                        <NavLink to="/author" className="btn btn-primary">Authors Section</NavLink>
                                        <button type="button" data-toggle="modal" data-target="#myModal" className="btn btn-danger" id="target" data-dismiss="modal" ref = { button => this.popupSubmit = button }>Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function bookOptions(booksData){    
    const options = booksData.map((book,index) =>{
        let option = <option value = { book.name } key = {(index+1).toString()}>{book.name}</option>
        return option;
    })
    return options;
}
export default connect(null,{ addAuthor })(NewAuthor);