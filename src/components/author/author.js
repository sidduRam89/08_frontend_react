import React from 'react';
import { NavLink } from 'react-router-dom';

class Author extends React.Component{
    constructor(props){
        super(props);
        this.handleDeleteChange = this.handleDeleteChange.bind(this);
        this.handleEditChange = this.handleEditChange.bind(this);
    }
    handleDeleteChange(event){
        console.log(event.currentTarget.value);
        this.props.deleteAuthor(event.currentTarget.value);
    }
    handleEditChange(event){
        this.props.getAuthorDataById(event.currentTarget.previousSibling.value);
    }
    render(){
        let elements = [];
        const { authorsData } = this.props;
        if(authorsData) {
            elements = createTbodyElement(authorsData, this.handleEditChange ,this.handleDeleteChange);
        }
        return(
            <div className ="table">
                <div className="header mt-5">
                    <h1 className="text-center">Authors Section</h1>       
                </div>
                <NavLink to="/newAuthor"  className ="btn fix-pos mt-3">New Author</NavLink>
                <table className="table table-striped mt-2">
                    <thead>
                        <tr>
                            <th scope="col">Author</th>
                            <th scope="col">Id</th>
                            <th scope="col">Name</th>
                            <th scope="col">Books</th>
                            <th scope="col" className="text-center">Mini Description</th>
                            <th scope="col">Occupations</th>
                            <th scope="col">Operation</th>
                        </tr>
                    </thead>
                    <tbody>
                    { elements }
                    </tbody>
                </table>
            </div>    
        );
    }
}

function createTbodyElement(authorsData, handleEditChange, handleDeleteChange){
    const elements = authorsData.map((authorData,index) => {
        let element = <tr key= {(index+1).toString()}>
            <td>
                <img src = { authorData['imgLink'] } alt={authorData['name']} />
            </td>
            <td className="id">{authorData['id']}</td>
            <td>{authorData['name']}</td>
            <td>{authorData['books'].join(',')}</td>
            <td className="text-justify">{authorData['description'].slice(0,150)}</td>
            <td>{authorData['occupation']}</td>
            <td>
                <p title="Update">
                <input type="hidden" name="id" value={authorData['id']} />
                <NavLink to="/updateAuthor" className="btn btn-primary btn-xs" onClick = { handleEditChange}><i className="fa fa-pencil"></i> </NavLink></p>
                <p title="Delete"><button className="btn btn-danger mt-2" value={authorData['id']} onClick= {handleDeleteChange}><i className="fa fa-trash"></i></button></p>
            </td>
        </tr>
        return element;
    });
    return elements;
}

export default Author;