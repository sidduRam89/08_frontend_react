/* eslint-disable no-console */
import {
  FETCH_BOOK, ADD_BOOK, EDIT_BOOK, DELETE_BOOK, GET_BOOK_BY_ISBN,
} from './actionType';

function bookAction(type, value) {
  return {
    type,
    payLoad: value,
  };
}
export function getBookByISBN(ISBN) {
  return (dispatch) => {
    fetch(`${process.env.REACT_APP_LIBRARY_API}/book/${ISBN}`,{
        mode:'cors',
      })
      .then(data => data.json())
      .then(bookData => dispatch(bookAction(GET_BOOK_BY_ISBN, bookData)))
      .catch(err => console.error(err));
  };
}
export function addBook(bookData) {
  return (dispatch) => {
    fetch(`${process.env.REACT_APP_LIBRARY_API}/book`, {
      method: 'POST',
      mode:'cors',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(bookData),
    })
      .then(_ => dispatch(bookAction(ADD_BOOK, bookData)))
      .catch(err => console.error(err));
  };
}
export function editBook(bookData) {
  const newBookData = { ...bookData };
  delete newBookData.authorName;
  return (dispatch) => {
    fetch(`${process.env.REACT_APP_LIBRARY_API}/book/${bookData.ISBN}`, {
      method: 'PUT',
      mode:'cors',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(newBookData),
    })
      .then(message => message.json())
      .then((data) => {
        console.log(data);
        dispatch(bookAction(EDIT_BOOK, bookData));
      })
      .catch(err => console.error(err));
  };
}
export function deleteBook(ISBN) {
  return (dispatch) => {
    fetch(`${process.env.REACT_APP_LIBRARY_API}/book/${ISBN}`, {
      method: 'DELETE',
      mode:'cors',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then(message => message.json())
      .then((data) => {
        console.log(data);
        dispatch(bookAction(DELETE_BOOK, ISBN));
      })
      .catch(err => console.error(err));
  };
}

export default function getBook() {
  return (dispatch) => {
    fetch(`${process.env.REACT_APP_LIBRARY_API}/book`,{
        mode:'cors',
      })
      .then(data => data.json())
      .then(booksData => dispatch(bookAction(FETCH_BOOK, booksData)))
      .catch(err => console.error(err));
  };
}
