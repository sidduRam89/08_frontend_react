/* eslint-disable no-undef */
/* eslint-disable no-console */

import {
  FETCH_AUTHOR, ADD_AUTHOR, EDIT_AUTHOR, DELETE_AUTHOR, GET_AUTHOR_BY_ID,
} from './actionType';

function authorAction(actionType, actionValue) {
  return {
    type: actionType,
    payLoad: actionValue,
  };
}

// get author by id
export const getAuthorDataById = id => (dispatch) => {
  fetch(`${process.env.REACT_APP_LIBRARY_API}/author/${id}`,{
      mode:'cors',
    })
    .then(data => data.json())
    .then((authorData) => {
      dispatch(authorAction(GET_AUTHOR_BY_ID, authorData));
    })
    .catch(err => console.error(err));
};

// add author
export function addAuthor(payload) {
  const authorData = { ...payload };
  delete authorData.book;
  console.log(authorData);
  return (dispatch) => {
    fetch(`${process.env.REACT_APP_LIBRARY_API}/author`, {
      method: 'POST',
      mode:'cors',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(authorData),
    })
      .then(response => response.json())
      .then((data) => {
        authorData.id = data.id;
        authorData.books = [payload.book];
        dispatch(authorAction(ADD_AUTHOR, authorData));
      })
      .catch(err => console.error(err));
  };
}

// delete author by id
export function deleteAuthor(id) {
  return (dispatch) => {
    fetch(`${process.env.REACT_APP_LIBRARY_API}/author/${id}`, {
      method: 'DELETE',
    })
      .then(data => data.json())
      .then(message => dispatch(authorAction(DELETE_AUTHOR, id)))
      .catch(err => console.error(err));
  };
}

// edit author data
export function editAuthor(authorData) {
  const newAuthorsData = { ...authorData };
  delete newAuthorsData.books;
  return (dispatch) => {
    fetch(`${process.env.REACT_APP_LIBRARY_API}/author/${authorData.id}`, {
      method: 'PUT',
      mode:'cors',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(newAuthorsData),
    })
      .then(response => response.json())
      .then((data) => {
        dispatch(authorAction(EDIT_AUTHOR, authorData));
      })
      .catch(err => console.error(err));
  };
}
// get overall authors data
export default function getAuthor() {
  return (dispatch) => {
    fetch(`${process.env.REACT_APP_LIBRARY_API}/author`,{
      mode:'cors',
    })
      .then(data => data.json())
      .then(authorsData => dispatch(authorAction(FETCH_AUTHOR, authorsData)))
      .catch(err => console.error(err));
  };
}
