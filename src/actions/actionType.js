export const ADD_AUTHOR = 'ADD_AUTHOR';
export const ADD_BOOK = 'ADD_BOOK';
export const DELETE_BOOK = 'DELETE_BOOK';
export const DELETE_AUTHOR = 'DELETE_AUTHOR';
export const EDIT_AUTHOR = 'EDIT_AUTHOR';
export const EDIT_BOOK = 'EDIT_BOOK';
export const FETCH_AUTHOR = 'FETCH_AUTHOR';
export const FETCH_BOOK = 'FETCH_BOOK';
export const GET_AUTHOR_BY_ID = 'GET_AUTHOR_BY_ID';
export const GET_BOOK_BY_ISBN = 'GET_BOOK_BY_ISBN';
