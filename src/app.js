import React from 'react';
import { BrowserRouter, Route} from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Navigation from './components/navigation';
import Book from './components/book/book';
import Author from './components/author/author';
import NewBook from './components/book/newBook';
import NewAuthor from './components/author/newAuthor';
import UpdateAuthor from './components/author/updateAuthor';
import UpdateBook from './components/book/updateBook';
import getAuthors, {deleteAuthor, getAuthorDataById} from './actions/authorAction';
import getBooks, { deleteBook, getBookByISBN } from './actions/bookAction';

class App extends React.Component{
    componentWillMount(){
        this.props.getAuthors();
        this.props.getBooks();
    }
    render(){
        return (
            <BrowserRouter>
                <div className="container">
                    <Navigation />
                    <div className ="contents">
                        <Route path='/book' component={() => <Book booksData = {this.props.booksData} deleteBook = { this.props.deleteBook } getBookByISBN ={this.props.getBookByISBN } />} />
                        <Route path="/author" component={() => <Author authorsData = {this.props.authorsData} deleteAuthor = { this.props.deleteAuthor} getAuthorDataById = {this.props.getAuthorDataById}  />} />
                        <Route path="/newBook" component ={() => <NewBook authorsData = {this.props.authorsData} /> }  />
                        <Route path="/newAuthor" component ={ () => <NewAuthor addAuthorSubmit ={this.addAuthorSubmit} booksData = {this.props.booksData}  /> }  />
                        <Route path="/updateAuthor" component ={ () => <UpdateAuthor authorData = {this.props.authorData} />} />
                        <Route path="/updateBook" component ={() =><UpdateBook authorsData = {this.props.authorsData} bookData = {this.props.bookData} />} />
                    </div>
                </div>
            </BrowserRouter>
        );
    }
}

App.propTypes = {
    getAuthors:PropTypes.func.isRequired,
    getBooks:PropTypes.func.isRequired,
    authorsData:PropTypes.array,
    booksData:PropTypes.array,
    authorData:PropTypes.object,
    bookData:PropTypes.object,
    bookISBN:PropTypes.number,
}

const mapStateToProps = (state) =>({
    authorsData:state.authorReducer.authorsData,
    authorData: state.authorReducer.authorData,
    booksData:state.bookReducer.booksData,
    bookData :state.bookReducer.bookData,
    bookISBN: state.bookReducer.bookISBN,
});
export default connect(mapStateToProps, { getAuthors, getBooks, deleteBook, deleteAuthor, getAuthorDataById, getBookByISBN })(App)
