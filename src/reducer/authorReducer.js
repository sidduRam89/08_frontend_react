import {
  FETCH_AUTHOR, ADD_AUTHOR, EDIT_AUTHOR, DELETE_AUTHOR, GET_AUTHOR_BY_ID,
} from '../actions/actionType';

const initialState = {
  authorsData: null,
  authorData: null,
  authorId: null,
};

function findAuthorById(authorsData, id) {
  let pos;
  authorsData.forEach((authorData, index) => {
    if (authorData.id === parseInt(id, 10)) {
      pos = index;
    }
  });
  return pos;
}
export default function authorReducer(state = initialState, action) {
  let newAuthorsData;
  let authorPos;
  switch (action.type) {
    case FETCH_AUTHOR:
      return { ...state, authorsData: action.payLoad };
    case ADD_AUTHOR:
      return { ...state, authorsData: state.authorsData.concat(action.payLoad) };
    case DELETE_AUTHOR:
      newAuthorsData = [...state.authorsData];
      authorPos = findAuthorById(newAuthorsData, action.payLoad);
      newAuthorsData.splice(authorPos, 1);
      return { ...state, authorsData: newAuthorsData };
    case GET_AUTHOR_BY_ID:
      newAuthorsData = { ...action.payLoad };
      authorPos = findAuthorById(state.authorsData, newAuthorsData.id);
      newAuthorsData.books = state.authorsData[authorPos].books;
      return { ...state, authorData: newAuthorsData };
    case EDIT_AUTHOR:
      newAuthorsData = [...state.authorsData];
      authorPos = findAuthorById(newAuthorsData, action.payLoad.id);
      newAuthorsData.splice(authorPos, 1, action.payLoad);
      return { ...state, authorsData: newAuthorsData };
    default:
      return state;
  }
}
