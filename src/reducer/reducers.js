import { combineReducers } from 'redux';
import authorReducer from './authorReducer';
import bookReducer from './bookReducer';

export default combineReducers({
  bookReducer,
  authorReducer,
});
