/* eslint-disable no-console */
import {
  FETCH_BOOK, ADD_BOOK, EDIT_BOOK, DELETE_BOOK, GET_BOOK_BY_ISBN,
} from '../actions/actionType';

const initialState = {
  booksData: null,
  bookData: null,
  bookISBN: null,
};
function findBookPosByISBN(booksData, ISBN) {
  let pos;
  booksData.forEach((value, index) => {
    if (value.ISBN === parseInt(ISBN, 10)) {
      pos = index;
    }
  });
  return pos;
}
export default function bookReducer(state = initialState, action) {
  let bookPos;
  let newBooksData;
  switch (action.type) {
    case FETCH_BOOK:
      return { ...state, booksData: action.payLoad };
    case ADD_BOOK:
      return { ...state, booksData: state.booksData.concat(action.payLoad) };
    case EDIT_BOOK:
      newBooksData = [...state.booksData];
      bookPos = findBookPosByISBN(newBooksData, action.payLoad.ISBN);
      newBooksData.splice(bookPos, 1, action.payLoad);
      return { ...state, booksData: newBooksData };
    case DELETE_BOOK:
      newBooksData = [...state.booksData];
      bookPos = findBookPosByISBN(state.booksData, action.payLoad);
      newBooksData.splice(bookPos, 1);
      return { ...state, booksData: newBooksData };
    case GET_BOOK_BY_ISBN:
      return { ...state, bookData: action.payLoad };
    default:
      return state;
  }
}
